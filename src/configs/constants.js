const constants                         = {
    callers                             : {

        PROPERTY_CREATE                 : "PROPERTY CREATE",

    },
    errors                              : {

        property                        :{
            ALREADY_EXISTS              : "The property exists with same details",
            INVALID_AIRBNBID            : "The airbnb id you entered is invalid",
            ERROR_IN_CREATING           : "Property cannot be created now, please try again",
            NOT_FOUND                   : "Property you requested for is not available",
            NO_PROPERTIES_FOUND         : "No properties found matching the criteria",
            NOT_FOUND_TO_UPDATE         : "Property you are trying to update is not found "
        }
    },
    success                             : {
        property                        :{
            CREATED                     : "Property created successfully",
            UPDATED                     : "Property updated successfully",
            DELETED                : "Property deleted successfully"
        }
    },
    db_values                           : {
        property                        : {
            status                      : {
                active                  : "active",
                verification_pending    : "verification_pending",
                blocked                 : "blocked"
            },
            airbnbStatus                : {
                pending                 : "pending",
                verified                : "verified",
                invalid                 : "invalid"
            }
        }
    }
};

module.exports = constants;