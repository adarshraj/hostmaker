const express       = require('express');
const bodyParse     = require('body-parser');
const routes        = require('./routes');
const configs       = require('./config');
const dataBase      = require('./database');
const hmLogger      = require('./logger');
const epValidation = require('express-validation');
const {error}         = require('./error');
const httpStatus    = require('http-status');
const connection    = dataBase.connect().catch(err =>
                        {
                            console.log(new Error("Problem in connecting to database"));
                            process.exit(0);
                        });
const app           = express();

const uuidv1        = require('uuid/v1');
app.use('*',(req,res,next) =>{
    req.traceId     = uuidv1();
    next();
});

app.use(bodyParse.json());
app.use(bodyParse.urlencoded({ extended: true }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

routes(app);

// if error is an instanceOf expressValidation , convert it.
app.use((err, req, res, next) => {
    if (err instanceof epValidation.ValidationError) {
        // validation error contains errors which is an array of error each containing message[]
        const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
        return error(res,{
            traceId : req.traceId,
            message : unifiedErrorMessage,
            status  : httpStatus.BAD_REQUEST
        })
    }
    return next(err);
});


app.use('/apidoc', express.static('doc'));

const PORT          = process.env.PORT || 3000;
app.listen(PORT, () =>{
    hmLogger.info({message : `Application started on PORT ${PORT}`});
});

module.exports = app;

