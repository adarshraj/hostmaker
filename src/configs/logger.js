const winston       = require('winston');
const configs       = require('./config').configs;
const logger        =  winston.createLogger({
    transports: [new winston.transports.Console({
            level: configs.LOG_LEVEL
        }
    )],
    format: winston.format.combine(
        winston.format.colorize({ all: true }),
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    )
});

const hmLogger          = {

    info                : ({endPoint = "" ,caller = "" ,type  = "",traceId = "" ,message  = "",status = ""}) =>{
        const logMsg    = `${status} ${endPoint}  ${traceId} ${caller} ${type}  ${JSON.stringify(message)}`;
        logger.info(logMsg);
    },
    error               : ({endPoint = "" ,caller = "" ,type  = "",traceId = "" ,message  = "",status = ""}) =>{
        const logMsg    = `${status} ${endPoint} ${traceId} ${caller} ${type}  ${JSON.stringify(message)}`;
        logger.error(logMsg);
    },
    debug               : ({endPoint = "" ,caller = "" ,type  = "",traceId = "" ,message  = "",status = ""}) =>{
        const logMsg    = `${status} ${endPoint}  ${traceId} ${caller} ${type}  ${JSON.stringify(message)}`;
        logger.debug(logMsg);
    }

};

module.exports          = hmLogger;
