require('dotenv').config();
const Joi                       = require('joi');

const configSchema              = Joi.object({

                                        NODE_ENV            : Joi.string().required()
                                                                            .allow("development","qa","production")
                                                                            .default("development"),
                                        MYSQL_HOST          : Joi.string().required(),
                                        MYSQL_PORT          : Joi.string().required(),
                                        MYSQL_DATABASE      : Joi.string().required(),
                                        MYSQL_USER          : Joi.string(),
                                        MYSQL_PASSWORD      : Joi.string(),
                                        LOG_LEVEL           : Joi.string(),
                                        API_HOST            : Joi.string().required(),
                                        PORT                : Joi.number().required()
                                    }).unknown().required();

const {error, value : configs}  = Joi.validate(process.env,configSchema);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}
exports.configs = configs;