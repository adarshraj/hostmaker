const configs            = require('./config').configs;
const dataBase           = require('./database');
const chai               = require('chai');
const expect             = chai.expect;

describe("Database Connection", ()=>{
    it ("Checks the database connection ", done => {
        dataBase.connect().
            then(connection => {
                expect(connection).
                    not.
                    equal(null);
                done();
            }).
            catch(error => {
                done(error);
            })
    });
});