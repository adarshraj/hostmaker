const httpStatus        = require('http-status');
const hmLogger          = require('./logger');
const success = (res,successInfo)=>{
    const response = {
        caller          : successInfo.caller,
        status          : successInfo.status || httpStatus.OK,
        message         : successInfo.message || "",
        timestamp       : new Date(),
        data            : successInfo.data || {}
    };

    hmLogger.info({
        endPoint        : successInfo.endPoint,
        caller          : successInfo.caller,
        type            : "RESPONSE",
        message         : successInfo.data,
        status          : successInfo.status,
        traceId         : successInfo.traceId
    });
    res.status(response.status).json(response);
};

exports.success = success;