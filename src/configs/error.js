const httpStatus        = require('http-status');
const hmLogger          = require('./logger');
const error = (res,errorInfo)=>{
    const response = {
        caller          : errorInfo.caller,
        status          : errorInfo.status || httpStatus.INTERNAL_SERVER_ERROR,
        message         : errorInfo.message || "",
        timestamp       : new Date(),
        data            : errorInfo.data || {}
    };

    hmLogger.error({
        endPoint        : errorInfo.endPoint,
        caller          : errorInfo.caller,
        type            : "RESPONSE",
        message         : errorInfo.message,
        status          : errorInfo.status,
        traceId         : errorInfo.traceId
    });

    res.status(response.status).json(response)
};

exports.error = error;