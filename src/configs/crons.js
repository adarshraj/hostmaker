const cron      = require('node-cron');
const database  = require('./database');
const hmLogger  = require('./logger');
const request   = require('request-promise');

/*
*
* Set up cronJob to every minute
* This job will query database for any properties which are in pending airbnb_verified ordered by created_at limit 1
*
*
* */
cron.schedule('* * * * *',async () => {

    const query        = "SELECT airbnb_id, id " +
                         " FROM properties" +
                         " WHERE  airbnb_verified = ? " +
                         " ORDER BY created_at LIMIT 1";
    const values       = ["pending"];

    var property     = await database.select({query,values});
    if(!property) {
        hmLogger.info({ message: "All properties are valid existing cronjob" });
        return;
    }

    const options  = {
        method: 'GET',
        url: 'https://www.airbnb.co.uk/rooms/'+property.airbnb_id
    };

    request(options)
    .then(function (response) {
        hmLogger.info({message : "Airbnb id is valid"});
        const query    = "UPDATE properties SET  airbnb_verified = ? status = ? WHERE id = ?";
        const values   = ["verified","active",property.id];
        const updated  =  database.update({query,values});
        return;

    })
    .catch(function (err) {
        hmLogger.error({message : "Error in fetching airbnb status code"});
        const query    = "UPDATE properties SET  airbnb_verified = ? WHERE id = ?";
        const values   = ["invalid",property.id];
        const updated  =  database.update({query,values});
        return;
    });

});