const configs            = require('./config').configs;
const chai               = require('chai');
const expect             = chai.expect;


describe("Configs", () => {

    it("validates NODE_ENV exists ", done => {
        expect(configs.NODE_ENV).to.be.a('string');
        done();
    });
    it("validates MYSQL_HOST exists ", done => {
        expect(configs.MYSQL_HOST).to.be.a('string');
        done();
    });
    it("validates MYSQL_PORT exists ", done => {
        expect(configs.MYSQL_PORT).to.be.a('string');
        done();
    });
    it("validates MYSQL_DATABASE exists ", done => {
        expect(configs.MYSQL_DATABASE).to.be.a('string');
        done();
    });
    it("validates MYSQL_USER exists ", done => {
        expect(configs.MYSQL_USER).to.be.a('string');
        done();
    });
    it("validates MYSQL_PASSWORD exists ", done => {
        expect(configs.MYSQL_PASSWORD).to.be.a('string');
        done();
    });

    it("validates LOG_LEVEL exists ", done => {
        expect(configs.LOG_LEVEL).to.be.a('string');
        done();
    });

    it("validates API_HOST exists ", done => {
        expect(configs.API_HOST).to.be.a('string');
        done();
    });

    it("validates port", done => {
        expect(configs.PORT).to.be.a('number');
        expect(configs.PORT).to.equal(3000);
        done();
    });
});
