const  mysql            = require('promise-mysql');
const configs           = require('./config').configs;
var connection;

const hmLogger          = require('./logger');

const dataBase          = {

    connect             : async () => {
        connection      = await mysql.createConnection({
                                host: configs.MYSQL_HOST,
                                user: configs.MYSQL_USER,
                                password: configs.MYSQL_PASSWORD,
                                database: configs.MYSQL_DATABASE
                            });
        hmLogger.debug({message:"Database connection initiated"});
        return connection;
    },
    select              : async ({query,values,logMsg}) => {
        hmLogger.debug({...logMsg,message:`SELECT QUERY ${query} ${values}`});
        const [rows]    = await connection.query(query, values);
        return rows;
    },
    selectAll           : async ({query,values}) =>{
        return await connection.query(query, values);
    },
    insert              : async ({query,values}) =>{
        return await connection.query(query,values);
    },
    update              : async ({query,values}) => {
        return           await connection.query(query,values);
    },
    delete              : async ({query,values}) => {
        return await connection.query(query,values);
    }
};
module.exports         = dataBase;

