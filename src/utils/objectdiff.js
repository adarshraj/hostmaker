const diff      = require('deep-diff').diff;

exports.diff    = (current,update) =>{
    return  diff(current, update);
};
