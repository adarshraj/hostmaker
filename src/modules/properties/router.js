const asyncHandler          = require('express-async-handler');
const express               = require('express');
const router                = express.Router();
const propertyController    = require('./controller');
const propertyValidators    = require('./validators');
const expressValidation     = require('express-validation');


/**
 * @api {get} properties/:id VIEW
 * @apiName GET PROPERTY
 * @apiGroup PROPERTIES
 *
 * @apiQuery {Number} id Property unique ID.
 *
 * @apiSampleRequest http://localhost:3000/properties/
 *
 * @apiSuccess {id} id  UNIQUE ID of the property.
 * @apiSuccess {String} owner  Owner details of the property.
 * @apiSuccess {Object} address Address of the property.
 * @apiSuccess {String} address.line1 Address Line 1.
 * @apiSuccess {String} address.line2 Address Line 2.
 * @apiSuccess {String} address.line3 Address Line 3.
 * @apiSuccess {String} address.line4 Address Line 4
 * @apiSuccess {String} address.city  City
 * @apiSuccess {String} address.country  Country
 * @apiSuccess {String} address.postcode Postcode
 * @apiSuccess {Number} airbnbId airbnbId of the property.
 * @apiSuccess {Number} numberOfBedrooms No of bed rooms in the  property.
 * @apiSuccess {Number} numberOfBathrooms No of bathrooms in the property.
 * @apiSuccess {String} status of the property (Active,Verification Pending, Blocked).
 * @apiSuccess {String} airbnbVerified Verification status with airbnb (Verified,Verification Failed)
 *
 * @apiSuccessExample {json} Success-Response:
 *
 *              {
                    "caller": "VIEW ALL PROPERTIES",
                    "status": 200,
                    "message": "",
                    "timestamp": "2018-04-30T09:52:09.029Z",
                    "data": [
                        {
                            "id": 2,
                            "owner": "carlos",
                            "address": {
                                "line1": "Flat 5",
                                "line2": "The Marina",
                                "line3": "Marina Towers",
                                "line4": "7 Westbourne Terrace",
                                "post_code": "W2 3UL",
                                "city": "London",
                                "country": "U.K."
                            },
                            "airbnbId": 3512507,
                            "numberOfBedrooms": 1,
                            "numberOfBathrooms": 1,
                            "status": "active",
                            "airbnbVerified": "Verified"
                        }
                    ]
                }
 *
 *
 *  @apiErrorExample {json} Error-Response:
 *
 *      Error 404: Not Found
         {
             "caller": "VIEW PROPERTY",
             "status": 404,
             "message": "Property you requested for is not available",
             "timestamp": "2018-04-30T09:52:57.818Z",
             "data": {}
         }

 */

/*
*
* express-async-handler is a library used to handle the response from a route.
* api Request validation are implemented using Joi library
*
* */

router.get('/:id',expressValidation(propertyValidators.viewProperty),asyncHandler(propertyController.view));


/**
 * @api {get} properties/ VIEW ALL
 * @apiName GET ALL PROPERTY
 * @apiGroup PROPERTIES
 *
 *
 *
 * @apiSampleRequest http://localhost:3000/properties
 *
 * @apiSuccess {id} id  UNIQUE ID of the property.
 * @apiSuccess {String} owner  Owner details of the property.
 * @apiSuccess {Object} address Address of the property.
 * @apiSuccess {String} address.line1 Address Line 1.
 * @apiSuccess {String} address.line2 Address Line 2.
 * @apiSuccess {String} address.line3 Address Line 3.
 * @apiSuccess {String} address.line4 Address Line 4
 * @apiSuccess {String} address.city  City
 * @apiSuccess {String} address.country  Country
 * @apiSuccess {String} address.postcode Postcode
 * @apiSuccess {Number} airbnbId airbnbId of the property.
 * @apiSuccess {Number} numberOfBedrooms No of bed rooms in the  property.
 * @apiSuccess {Number} numberOfBathrooms No of bathrooms in the property.
 * @apiSuccess {String} status of the property (Active,Verification Pending, Blocked).
 * @apiSuccess {String} airbnbVerified Verification status with airbnb (Verified,Verification Failed)
 *
 * @apiSuccessExample {json} Success-Response:
 *
 *             {
                "caller": "VIEW ALL PROPERTIES",
                "status": 202,
                "message": "",
                "timestamp": "2018-04-30T11:59:50.020Z",
                "data": [
                    {
                        "id": 2,
                        "owner": "carlos",
                        "address": {
                            "line1": "Flat 5",
                            "line2": "The Marina",
                            "line3": "Marina Towers",
                            "line4": "7 Westbourne Terrace",
                            "post_code": "W2 3UL",
                            "city": "London",
                            "country": "U.K."
                        },
                        "airbnbId": 3512507,
                        "numberOfBedrooms": 1,
                        "numberOfBathrooms": 1,
                        "status": "active",
                        "airbnbVerified": "Verified"
                    },
                    {
                        "id": 3,
                        "owner": "carlos",
                        "address": {
                            "line1": "Flat 5",
                            "line2": "The Marina",
                            "line3": "Marina Towers",
                            "line4": "7 Westbourne Terrace",
                            "post_code": "W2 3UL",
                            "city": "London",
                            "country": "U.K."
                        },
                        "airbnbId": 35125011,
                        "numberOfBedrooms": 1,
                        "numberOfBathrooms": 1,
                        "status": "active",
                        "airbnbVerified": "Verified"
                    }
                ]
            }
 *
 *
 *  @apiErrorExample {json} Error-Response:
 *
 *      Error 404: Not Found
         {
             "caller": "VIEW ALL PROPERTIES",
             "status": 404,
             "message": "No properties found matching the criteria",
             "timestamp": "2018-04-30T12:00:16.001Z",
             "data": []
         }

 */

/*
*
* express-async-handler is a library used to handle the response from a route.
* api Request validation are implemented using Joi library
*
* */

router.get('/',expressValidation(propertyValidators.viewAllProperty),asyncHandler(propertyController.viewAll));

/**
 * @api {post} properties/ NEW PROPERTY
 * @apiName CREATE NEW PROPERTY
 * @apiGroup PROPERTIES
 *
 * @apiParam {String} owner Owner name
 * @apiParam {Object} address Address of the property
 * @apiParam {String} address.line1 Address line 1
 * @apiParam {String} [address.line2]  Address line 2
 * @apiParam {String} [address.line3]  Address line 3
 * @apiParam {String} address.line4  Address line 4
 * @apiParam {String} address.postCode  postCode
 * @apiParam {String} address.city  city
 * @apiParam {String} address.country country
 * @apiParam {Number} airbnbId airbnbId
 * @apiParam {Number} numberOfBedrooms numberOfBedrooms (mandatory & >=0 )
 * @apiParam {Number} numberOfBathrooms numberOfBedrooms (mandatory &>0)
 * @apiParam {Number} incomeGenerated incomeGenerated (Required & >0)
 *
 *
 *  @apiSampleRequest http://localhost:3000/properties/
 *
 *
 *
 *  @apiSuccessExample {json} Success-Response:
 *
 *   {
        "caller": "PROPERTY CREATE",
        "status": 202,
        "message": "Property created successfully",
        "timestamp": "2018-04-30T14:47:00.429Z",
        "data": {
            "id": 5
        }
    }
 *
 *
 *  @apiErrorExample {json} Error-Response:
 *
 *  {
        "caller": "PROPERTY CREATE",
        "status": 409,
        "message": "The property exists with same details",
        "timestamp": "2018-04-30T14:46:42.420Z",
        "data": {}
    }
 *
 * */



router.post('/',expressValidation(propertyValidators.newProperty),asyncHandler(propertyController.create));


/**
 * @api {put} properties/ UPDATE PROPERTY
 * @apiName UPDATE EXISTING PROPERTY
 * @apiGroup PROPERTIES
 *
 * @apiParam {id} id  Unique id of the property
 * @apiParam {String} owner Owner name
 * @apiParam {Object} address Address of the property
 * @apiParam {String} address.line1 Address line 1
 * @apiParam {String} [address.line2]  Address line 2
 * @apiParam {String} [address.line3]  Address line 3
 * @apiParam {String} address.line4  Address line 4
 * @apiParam {String} address.postCode  postCode
 * @apiParam {String} address.city  city
 * @apiParam {String} address.country country
 * @apiParam {Number} airbnbId airbnbId
 * @apiParam {Number} numberOfBedrooms numberOfBedrooms (mandatory & >=0 )
 * @apiParam {Number} numberOfBathrooms numberOfBedrooms (mandatory &>0)
 * @apiParam {Number} incomeGenerated incomeGenerated (Required & >0)
 *
 *
 *  @apiSampleRequest http://localhost:3000/properties/
 *
 *
 *
 *  @apiSuccessExample {json} Success-Response:
 *
 *   {
        "caller": "UPDATE PROPERTY",
        "status": 202,
        "message": "Property updated successfully",
        "timestamp": "2018-04-30T14:49:38.158Z",
        "data": {}
    }
 *
 *
 *  @apiErrorExample {json} Error-Response:
 *
 *  {
        "caller": "UPDATE PROPERTY",
        "status": 404,
        "message": "Property you are trying to update is not found ",
        "timestamp": "2018-04-30T14:50:07.966Z",
        "data": {}
    }
 *
 * */


router.put('/:id',expressValidation(propertyValidators.updateProperty),asyncHandler(propertyController.update));


/**
 * @api {delete} properties/ DELETE PROPERTY
 * @apiName DELETE A PROPERTY
 * @apiGroup PROPERTIES
 *
 *  @apiParam {id} id  Unique id of the property
 *
 *  @apiSampleRequest http://localhost:3000/properties/
 *
 *  @apiSuccessExample {json} Success-Response:
 *
 *  {
        "caller": "DELETE PROPERTY",
        "status": 202,
        "message": "Property deleted successfully",
        "timestamp": "2018-04-30T14:53:19.369Z",
        "data": {}
    }
 *
 * @apiErrorExample {json} Error-Response:
 *
 *  {
        "caller": "DELETE PROPERTY",
        "status": 404,
        "message": "Property you requested for is not available",
        "timestamp": "2018-04-30T14:52:46.113Z",
        "data": {}
    }
 *
 * */

router.delete('/:id',expressValidation(propertyValidators.deleteProperty),asyncHandler(propertyController.delete));
module.exports = router;