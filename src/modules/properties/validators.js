const Joi                       = require('joi');

const validators                = {


    newProperty                 : {

        body                    : {

            owner               : Joi.string().required(),
            address             : Joi.object().keys({
                line1           : Joi.string().required(),
                line2           : Joi.string(),
                line3           : Joi.string(),
                line4           : Joi.string().required(),
                postCode        : Joi.string().required(),
                city            : Joi.string().required(),
                country         : Joi.string().required()
            }).required(),
            airbnbId            : Joi.number().integer().required(),
            numberOfBedrooms    : Joi.number().integer().required().min(0),
            numberOfBathrooms   : Joi.number().integer().required().min(1),
            incomeGenerated     : Joi.number().required().greater(0)
        }
    },
    updateProperty              : {
        params                  : {
            id                  : Joi.string().required(),
        },
        body                    : {
            owner               : Joi.string().required(),
            address             : Joi.object().keys({
                line1           : Joi.string().required(),
                line2           : Joi.string(),
                line3           : Joi.string(),
                line4           : Joi.string().required(),
                postCode        : Joi.string().required(),
                city            : Joi.string().required(),
                country         : Joi.string().required()
            }).required(),
            airbnbId            : Joi.number().integer().required(),
            numberOfBedrooms    : Joi.number().integer().required().min(0),
            numberOfBathrooms   : Joi.number().integer().required().min(1),
            incomeGenerated     : Joi.number().required().greater(0)
        }
    },
    viewProperty                : {
        params                  : {
            id                  : Joi.string().required()
        }
    },
    viewAllProperty             : {

    },
    deleteProperty              : {
        params                  : {
            id                  : Joi.string().required()
        }
    }


};

module.exports      = validators;