const request = require('supertest-as-promised');
const httpStatus  = require('http-status');
const chai  = require('chai');
const expect = require('chai').expect;
const app = require('../../configs/app');
console.log(app);

describe('## Properties API Tests', () => {

    describe('### GET /properties', () => {

        it('should return all properties', (done) => {
            request(app)
            .get('/properties/')
            .send()
            .expect(httpStatus.OK)
            .then(res => {
                expect(res.body.data).to.exist;
                done();
            });
        });

    });

    describe('### GET /properties/:id', () => {

        it('should return a property', (done) => {
            request(app)
            .get('/properties/1')
            .send()
            .expect(httpStatus.OK)
            .then(res => {
                expect(res.body.data).to.exist;
                done();
            });
        });

    });

    describe('### POST /properties', () => {


        it('should return the created property successfully', (done) => {
            request(app)
            .post('/properties/')
            .send({
                "owner": "carlos",
                "address": {
                    "line1": "Flat 5",
                    "line2":"The Marina",
                    "line3":"Marina Towers",
                    "line4": "7 Westbourne Terrace",
                    "postCode": "W2 3UL",
                    "city": "London",
                    "country": "U.K."
                },
                "airbnbId": Math.floor(Math.random()*90000) + 10000,
                "numberOfBedrooms": 1,
                "numberOfBathrooms": 1,
                "incomeGenerated": 2000.34
            })
            .expect(httpStatus.ACCEPTED)
            .then(res => {
                expect(res.body.message).to.equal('Property created successfully');
                expect(res.body.data.id).to.exist;
                done();
            });
        });

        it('should return the error while creating property', (done) => {
            request(app)
            .post('/properties/')
            .send({
                "owner": "carlos",
                "address": {
                    "line1": "Flat 5",
                    "line2":"The Marina",
                    "line3":"Marina Towers",
                    "line4": "7 Westbourne Terrace",
                    "postCode": "W2 3UL",
                    "city": "London"
                },
                "airbnbId": Math.floor(Math.random()*90000) + 10000,
                "numberOfBedrooms": 1,
                "numberOfBathrooms": 1,
                "incomeGenerated": 2000.34
            })
            .expect(httpStatus.BAD_REQUEST)
            .then(res => {
                done();
            });
        });


    });

    describe('### PUT /properties/:id', () => {

        it('should update a property', (done) => {
            request(app)
            .put('/properties/1')
            .send({
                "owner": "carlos",
                "address": {
                    "line1": "Flat 5",
                    "line2":"The Marina",
                    "line3":"Marina Towers",
                    "line4": "7 Westbourne Terrace",
                    "postCode": "W2 3UL",
                    "city": "London",
                    "country": "U.K."
                },
                "airbnbId": Math.floor(Math.random()*90000) + 10000,
                "numberOfBedrooms": 1,
                "numberOfBathrooms": 1,
                "incomeGenerated": 2000.34
            })
            .expect(httpStatus.ACCEPTED)
            .then(res => {
                done();
            });
        });

    });

    describe('### DELETE /properties/:id', () => {

        it('should update a property', (done) => {
            request(app)
            .delete('/properties/1')
            .send()
            .expect(httpStatus.ACCEPTED)
            .then(res => {
                done();
            });
        });
    });
});