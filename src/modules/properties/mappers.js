const objectMapper          = require('object-mapper');

const propertyMapper        = {

    toDBModel               : (source) =>{

        const dbMap         = {
            "id"            : {
                key         : "id",
                transform   : function (value) {
                    return value;
                }
            },
            "owner"         : {
                key         : "owner",
                transform   : function (value) {
                    return value;
                }
            },
            "address.line1" : {
                key         : "address_line_1",
                transform   : function (value) {
                    return value;
                }
            },
            "address.line2" : {
                key         : "address_line_2",
                transform: function (value) {
                    return value || null;
                }
            },
            "address.line3" : {
                key         : "address_line_3",
                transform: function (value) {
                    return value || null;
                }
            },
            "address.line4" : {
                key         : "address_line_4",
                transform   : function (value) {
                    return value;
                }
            },
            "address.postCode"     : {
                key         : "post_code",
                transform   : function (value) {
                    return value;
                }
            },
            "address.city"  : {
                key         : "city",
                transform   : function (value) {
                    return value;
                }
            },
            "address.country": {
                key         : "country",
                transform   : function (value) {
                    return value;
                }
            },
            "airbnbId"      : {
                key         : "airbnb_id",
                transform   : function (value) {
                    return value;
                }
            },
            "numberOfBedrooms": {
                key         : "no_of_bedrooms",
                transform   : function (value) {
                    return value;
                }
            },
            "numberOfBathrooms": {
                key          : "no_of_bathrooms",
                transform   : function (value) {
                    return value;
                }
            },
            "incomeGenerated": {
                key          : "income_generated",
                transform   : function (value) {
                    return value;
                }
            },
        };
        return datatoSave   = objectMapper(source, dbMap);

    },
    toResponseModel         :  (source)=>{
        const responseMap   = {

            "id"            : {
                key         : "id",
                transform   : function (value) {
                    return value;
                }
            },
            "owner"         : {
                key         : "owner",
                transform   : function (value) {
                    return value;
                }
            },
            "address_line_1": {
                key         : "address.line1",
                transform   : function (value) {
                    return value;
                }
            },
            "address_line_2": {
                key         : "address.line2",
                transform: function (value) {
                    return value || null;
                }
            },
            "address_line_3": {
                key         : "address.line3",
                transform: function (value) {
                    return value || null;
                }
            },
            "address_line_4": {
                key         : "address.line4",
                transform   : function (value) {
                    return value;
                }
            },
            "post_code"     : {
                key         : "address.post_code",
                transform   : function (value) {
                    return value;
                }
            },
            "city"          : {
                key         : "address.city",
                transform   : function (value) {
                    return value;
                }
            },
            "country"       : {
                key         : "address.country",
                transform   : function (value) {
                    return value;
                }
            },
            "airbnb_id"     : {
                key         : "airbnbId",
                transform   : function (value) {
                    return value;
                }
            },
            "no_of_bedrooms": {
                key         : "numberOfBedrooms",
                transform   : function (value) {
                    return value;
                }
            },
            "no_of_bathrooms": {
                key          : "numberOfBathrooms",
                transform   : function (value) {
                    return value;
                }
            },
            "status"        : {
                key         : "status",
                transform   : function (value) {
                    return value;
                }
            },
            "airbnb_verified": {
                key          : "airbnbVerified",
                transform   : function (value) {
                    return value;
                }
            },

        };
        return response     = objectMapper(source, responseMap);

    }
};

module.exports          = propertyMapper;

