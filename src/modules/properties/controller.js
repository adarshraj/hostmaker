const propertyHelper            = require('./helpers');
const httpStatus                = require('http-status');
const {success}                 = require('../../configs/success');
const {error}                   = require('../../configs/error');
const constants                 = require('../../configs/constants');
const mappers                   = require('./mappers');
const {diff}                    = require('../../utils/objectdiff');

const hmLogger                  = require('../../configs/logger');

const propertyContoller         = {

    create                      : async (req,res,next) => {

        const caller            = "PROPERTY CREATE";

        const endPoint          = "POST /properties";
        const logMsg            = {
                                    endPoint : endPoint,
                                    traceId  : req.traceId,
                                    caller   : caller
                                };

        const data              = mappers.toDBModel(req.body);      // Mapping request to Database model

        hmLogger.info({...logMsg,type : "REQUEST", message : data });
        /*
        *
        * Exists check can be added with other params too, function need to be modified here
        *
        * TODO:  existing checks currently checks only airbnbId
        * */
        const isExistAlready    = await propertyHelper.isExists(data, "airbnbId"); // Checking propety exists with the same airbnbID

        hmLogger.info({...logMsg, message : `Propert exists check ${isExistAlready}` });

        if (isExistAlready)
            return error(res, {
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.CONFLICT,
                message         : constants.errors.property.ALREADY_EXISTS,
                data            : null
            });
        /**
         *
         *  IMP: airbnb check is done with background job
         *
         */

        /*const airBnbValid       = await propertyHelper.airbnbIdValid(data);
        if (!airBnbValid)
            return error(res, {
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.PRECONDITION_FAILED,
                message         : constants.errors.property.INVALID_AIRBNBID,
                data            : null
            });*/
        data.airbnb_verified    = constants.db_values.property.airbnbStatus.pending;            // initial airbnb status as pending
        data.status             = constants.db_values.property.status.verification_pending;     // initail status as verification pending

        const id                = await propertyHelper.create(data);                // id returned after insertion
        if (!id)
            return error(res, {
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.INTERNAL_SERVER_ERROR,
                message         : constants.errors.property.ERROR_IN_CREATING,
                data            : null
            });

        return success(res, {
            endPoint            : endPoint,
            caller              : caller,
            traceId             : req.traceId,
            status              : httpStatus.ACCEPTED,
            message             : constants.success.property.CREATED,
            data                : {
                                    id: id
                                  }
        });
    },
    view                        : async (req,res,next) => {

        const caller            = "VIEW PROPERTY";
        const endPoint          = "GET properties/:id";
        const logMsg            = {
                                        endPoint : endPoint,
                                        traceId         : req.traceId,
                                        caller   : caller
                                    };
        const data              = {
                                    id : req.params.id
                                  };
        hmLogger.info({...logMsg,type : "REQUEST", message : data });
        /*
        *   logMsg is passed down to the function calls for log tracing
        * */
        const property          = await propertyHelper.getOne(data,logMsg);     // Getting property details from DB
        if(!property)                                                           // When details not FOUND
            return error(res,{
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.NOT_FOUND,
                message         : constants.errors.property.NOT_FOUND,
                data            : null
            });
        return success(res, {
            endPoint        : endPoint,
            caller          : caller,
            traceId         : req.traceId,
            status              : httpStatus.OK,
            // data                : property
            data                : mappers.toResponseModel(property)         // reverse mapping of the database values to response
        });
    },
    viewAll                     : async (req,res,next) => {
        const caller            = "VIEW ALL PROPERTIES";
        const endPoint          = "GET /properties";
        const logMsg            = {
                                        endPoint : endPoint,
                                        traceId         : req.traceId,
                                        caller   : caller
                                    };
        const data              = {};
        hmLogger.info({...logMsg,type : "REQUEST", message : data });
        const properties        = await propertyHelper.getAll(data);
        if(!properties)
            return error(res,{
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.NOT_FOUND,
                message         : constants.errors.property.NO_PROPERTIES_FOUND,
                data            : []
            });
        const mappedProperties  = properties.reduce((mappedData,array)=> {
                                        mappedData.push(mappers.toResponseModel(array));
                                        return mappedData;
                                    },[]);
        return success(res, {
            endPoint            : endPoint,
            caller              : caller,
            traceId             : req.traceId,
            status              : httpStatus.OK,
            // data                : property
            data                : mappedProperties
        });
    },
    update                      : async (req,res,next) => {

        const caller            = "UPDATE PROPERTY";
        const endPoint          = "PUT /properties/:id";
        const logMsg            = {
                                        endPoint : endPoint,
                                        traceId         : req.traceId,
                                        caller   : caller
                                    };
        let data                = mappers.toDBModel(req.body);          // Mapping Request to database models
        data.id                 = Number.parseInt(req.params.id);

        hmLogger.info({...logMsg,type : "REQUEST", message : data });

        const isExistAlready    = await propertyHelper.isExists(data);

        hmLogger.info({...logMsg, message : `Propert exists check ${isExistAlready}` });

        if (!isExistAlready)
            return error(res, {
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.NOT_FOUND,
                message         : constants.errors.property.NOT_FOUND_TO_UPDATE,
                data            : null
            });

        const currentVersion    = await propertyHelper.getOne(data);
        delete currentVersion.created_at;
        delete currentVersion.modified_at;

        /*
        *
        * IF airbnb id is not changed then no need to verify again
        *
        * */

        if(currentVersion.airbnb_id !== data.airbnb_id){
            data.airbnb_verified = constants.db_values.property.airbnbStatus.pending;
            data.status         =  constants.db_values.property.status.verification_pending;
        }else{
            data.airbnb_verified= currentVersion.airbnb_verified;
            data.status         = currentVersion.status;
        }
        const update            = await propertyHelper.update(data);
        const difference        = diff(currentVersion,data) || [];      // Find out the difference between old and new object saved in DB as json
        const differenceToSave  = difference.reduce((reduced,array) => {
                                              reduced[array.path[0]] = {
                                                  old_value : array.lhs,
                                                  new_value : array.rhs
                                              };
                                            return reduced;
                                    },{});
        if(Object.keys(differenceToSave).length >0 ) {
            const versionId     = await propertyHelper.createVersion(currentVersion, update, JSON.stringify(differenceToSave));
        }

        return success(res, {
            endPoint            : endPoint,
            caller              : caller,
            traceId             : req.traceId,
            status              : httpStatus.ACCEPTED,
            message             : constants.success.property.UPDATED,
        });

    },
    delete                      : async (req,res,next) => {

        const caller            = "DELETE PROPERTY";
        const endPoint          = "DELETE /properties/:id";
        const logMsg            = {
                                        endPoint : endPoint,
                                        traceId         : req.traceId,
                                        caller   : caller
                                    };
        const data              = {id : req.params.id};

        hmLogger.info({...logMsg,type : "REQUEST", message : data });

        const isExistAlready    = await propertyHelper.isExists(data);
        if (!isExistAlready)
            return error(res, {
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.NOT_FOUND,
                message         : constants.errors.property.NOT_FOUND,
                data            : null
            });

        const deleted           = await propertyHelper.delete(data);
        if(!deleted)
            return error(res, {
                endPoint        : endPoint,
                caller          : caller,
                traceId         : req.traceId,
                status          : httpStatus.INTERNAL_SERVER_ERROR,
                message         : constants.errors.property.ERROR_CREATING_PROPERTY,
                data            : null
            });

        return success(res, {
            endPoint            : endPoint,
            caller              : caller,
            traceId             : req.traceId,
            status              : httpStatus.ACCEPTED,
            message             : constants.success.property.DELETED,
        });
    }
};



module.exports                 = propertyContoller;

/*exports.create = async (req,res,next)=>{

    const caller                 = "PROPERTY CREATE";
    const data                   = {
                                        owner           : req.body.owner,
                                        addressOne      : req.body.address.line1,
                                        addressTwo      : req.body.address.line2 || null,
                                        addressThree    : req.body.address.line3 || null,
                                        addressFour     : req.body.address.line4,
                                        postCode        : req.body.address.postCode,
                                        city            : req.body.address.city,
                                        country         : req.body.address.country,
                                        noOfBedRooms    : req.body.numberOfBedrooms,
                                        noOfBathRooms   : req.body.numberOfBathrooms,
                                        airbnbId        : req.body.airbnbId,
                                        incomeGenerated : req.body.incomeGenerated
                                    };
    const isExistAlready         = await propertyHelper.isExists(data);
    if(isExistAlready)
           return error(res,{
                                caller  : caller,
                                status  : httpStatus.CONFLICT,
                                message : constants.errors.property.ALREADY_EXISTS,
                                data    : null
                        });
    const airBnbValid           = await propertyHelper.airbnbIdValid(data);
    if(!airBnbValid)
            return error(res,{
                                caller  : caller,
                                status  : httpStatus.PRECONDITION_FAILED,
                                message : constants.errors.property.INVALID_AIRBNBID,
                                data    : null
                        });
    data.airbnb_verified        = airBnbValid;
    data.status                 = airBnbValid ?
                                                constants.db_values.property.status.active :
                                                constants.db_values.property.status.verification_pending;

    const id                = await propertyHelper.create(data);
    if(!id)
            return error(res, {
                                caller  : caller,
                                status  : httpStatus.INTERNAL_SERVER_ERROR,
                                message : constants.errors.property.ERROR_CREATING_PROPERTY,
                                data    : null
                        });

    return success(res, {
                                caller  : caller,
                                status  : httpStatus.ACCEPTED,
                                message : constants.success.property.CREATED,
                                data    : {
                                    id  : id
                                }
                        });

};

exports.view  = async (req,res,next)=>{

};

exports.update = async(req,res,next)=>{

};

exports.delete = async(req,res,next)=>{

};*/

