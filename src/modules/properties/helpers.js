const database                      = require('../../configs/database');


const propertyHelper  = {

    isExists                        : async (data,type = "id")   =>{
        let query                   = "SELECT count(id) as existCount " +
                                      "FROM properties " ;

        const values                = [];
        if(type == "airbnbId") {
            values.push(data.airbnb_id);
            query                   = query+ "WHERE airbnb_id = ?";
        }
        if(type == "id"){
            query                   = query+ "WHERE id = ?";
            values.push(data.id);
        }
        const existingData          = await database.select({query,values});
        return existingData.existCount > 0;
    },
    airbnbIdValid                   : async (data)   =>{
        return true;
    },
    create                          : async (data) =>{
        const query                 = "INSERT INTO properties SET ?" ;
        const property              = data;
        const propertyInserted      = await database.insert({query,values : property});
        return propertyInserted !== undefined  ? propertyInserted.insertId : null;

    },
    getOne                            : async (data,logMsg = {})  =>{

        const query                 = "SELECT * FROM properties WHERE id =?";
        const values                = [data.id];
        const property              = await database.select({query,values,logMsg});
        return property !== undefined ? property : null;
    },
    getAll                          : async (data) =>{
        const query                 = "SELECT * FROM properties";
        const values                = [];
        const properties            = await database.selectAll({query,values});
        return properties.length > 0 ? properties : null;

    },
    update                          : async (data) =>{
        const query                 = "UPDATE properties SET owner = ?, address_line_1 =? , address_line_2 = ?," +
                                        " address_line_3 =?,address_line_4 = ?, post_code = ? , city = ? , country = ?, airbnb_id = ?, " +
                                        "income_generated = ?,airbnb_verified = ?, no_of_bedrooms = ?, no_of_bathrooms = ? " +
                                        "WHERE id = ? ";

        const values                = [
                                            data.owner,
                                            data.address_line_1,
                                            data.address_line_2,
                                            data.address_line_3,
                                            data.address_line_4,
                                            data.post_code,
                                            data.city,
                                            data.country,
                                            data.airbnb_id,
                                            data.income_generated,
                                            data.airbnb_verified,
                                            data.no_of_bedrooms,
                                            data.no_of_bathrooms,
                                            data.id
                                        ];
        const updatedStatus         = await database.update({query,values});
        return updatedStatus.affectedRows > 0;

    },
    createVersion                   : async (current,updates,diff) =>{
        const query                 = "INSERT INTO property_revisions SET ?";
        const values                = Object.assign(current, {property_id : current.id,diff: diff});
        delete values.id;
        const propertyVersion       = await database.insert({query,values});
        return propertyVersion !== undefined  ? true : false;
    },
    delete                          : async (data) =>{
        const query                 = "DELETE  FROM properties WHERE id = ?";
        const values                = [data.id];
        const deleted               = await database.delete({query,values});
        return deleted ? true : false;
    }
};

module.exports        = propertyHelper;