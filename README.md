For this exercise:


Please use any choice of technology. We would prefer it to be Node.js and RDBMS (mysql or postgres).
Please use any choice of architecture pattern � either a standard server side rendered web-page or
Web-Api that is used by JavaScript on the page. We would like to see if you can reason your choice.
Please try do have your code unit-tested or have some form of automated testing in it.
Please do use a version-control system (preferably git -- you can do share it over bitbucket or just zip the
whole folder including the. git folder)
Attached is the json file with the data in. (exerciseData.json)
Background: As an Account manager at HostMaker I want to be able to manage all property details.
Story 1: As an Account manager at HostMaker I want to be able to Get/Create/Update/Delete the property
details.
For this exercise the property details have following fields:
1) Host(Required)
2) Address
a. Line1 (Required)
b. Line2 (Optional)
c. Line3 (Optional)
d. Line4 (Required)
e. Postcode (Required)
f. City (Required)
g. Country (Required)
3) NumberOfBedrooms (mandatory & >=0 )
4) NumberOfBathrooms (mandatory &>0)
5) AirbnbID (must have a valid airbnb_id. Example: 2354700 is a valid airbnb-id because when we go
to the link. We get a https://www.airbnb.co.uk/rooms/2354700, 200-OK response. Whereas if we
go to something random like https://www.airbnb.co.uk/rooms/242424242424 - we get 302. So
ideally we would like the server to ping airbnb and check if it exists. Airbnb does block repeated
http requests from same IP. So might want to consider a strategy to handle that. AirbnbId's dont
change that often. )
6) IncomeGenerated (Required & >0)
We need to publish restful Api/s to manage this data. For creating and updating properties, we need to
make sure the persisted data is valid. So ensure that business rules are respected.



## Solution


Wrote a set of REST apis to CREAT, VIEW , UPDATE, DELETE properties with Node.js


##### Getting Started

Source file can be found under **src** folder, modules are seperated based on business entity. So only one module **properties** in this code base

      | -- src
            | -- configs
                   | -- app.js
                   | -- config.js
                   | -- config.spec.js
                   | -- constants.js
                   | -- database.js
                   | -- database.spec.js
                   | -- error.js
                   | -- success.js
                   | -- routes.js
                   | -- logger.js
            | -- modules
                   | -- properties
                      | -- controller.js
                      | -- helpers.js
                      | -- mappers.js
                      | -- properties.spec.js
                      | -- router.js
                      | - validators.js
            | -- utils
                   | -- objectdiff.js
            | -- index.js

      | -- dist
      | -- .env
      | -- database.sql
      | -- package.json
      | -- README.md




##### How to run


     Config .env file
     Run command npm install
     Run command npm start -s



### Documentation


    Run the application
    visit http://localhost:3000/apidoc



##### How airbnb requests are handled

Airbnb status check is happening via scheduled task, it checks only one property status in every minute.

#### Running test cases

      Run command npm run test

##### Sample Api Request

    Sample api request can be found at


    https://www.getpostman.com/collections/fce5ba530203a1a3e1b7
